<?php

namespace App\Repository;

use App\Entity\Mecanique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Mecanique|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mecanique|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mecanique[]    findAll()
 * @method Mecanique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MecaniqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mecanique::class);
    }

    // /**
    //  * @return Mecanique[] Returns an array of Mecanique objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Mecanique
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
