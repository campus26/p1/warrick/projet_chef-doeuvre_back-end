<?php

namespace App\Repository;

use App\Entity\Interieur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Interieur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Interieur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Interieur[]    findAll()
 * @method Interieur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterieurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Interieur::class);
    }

    // /**
    //  * @return Interieur[] Returns an array of Interieur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Interieur
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
