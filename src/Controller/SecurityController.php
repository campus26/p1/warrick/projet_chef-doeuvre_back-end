<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class SecurityController extends AbstractController
{
    /**
     *  @Route("/", name="api_index_user")
     */
    public function index(): Response
    {
        return $this->redirectToRoute('api_get_user');
    }
   /**
    * @Route("/login", name="api_get_user", methods={"POST"})
    */
    public function apiGetUser(Request $request, UserPasswordHasherInterface $hashedPassword): Response
    {
        // via form-data
        // $email = $request->get('email');
        // $password = $request->get('password');

        // via json
        $datas = $request->toArray();
        $email = $datas['email'];
        $password = $datas['password'];

        // get user with email
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(['email' => $email]);
       
        if(is_null($user)) {
            return $this->json('pas ok');
        }

        // compare hashed password
        if (!$hashedPassword->isPasswordValid($user, $password)) {
            return $this->json('pas ok');

        }
        return $this->json($user->getApiToken());
    }

    public function __construct(UserPasswordHasherInterface $hashedPassword)
    {
        $this->hashedPassword = $hashedPassword;
    }

    /**
    * @Route("/register", name="api_post_user", methods={"POST"})
    */
    public function apiPostUser(Request $request, ValidatorInterface $validator)
    {
        // Token: -> https://thisinterestsme.com/generating-random-token-php/
        // Generate a random string.
        $string = openssl_random_pseudo_bytes(16);
        //Convert the binary data into hexadecimal representation.
        $string = bin2hex($string);
        $timestamp = mt_rand(1609459200, time());
        $formattedDateTime = date("Y-m-d h:i:s", $timestamp);

        // via json
        $datas = $request->toArray();
        $emailReceived = $datas['email'];
        $passwordReceived = $datas['password'];

        $entityManager = $this->getDoctrine()->getManager();

        // simple user
        $tokenUser = sha1(mt_rand(0,1000)."plop".$formattedDateTime.$string);
        $user = new User();
        $user->setRoles(['ROLE_USER']);
        $user->setEmail($emailReceived);
        $user->setPassword($this->hashedPassword->hashPassword(
            $user,
            $passwordReceived
        ));

        $errors = $validator->validate($user);
            if (count($errors) > 0) {
                /*
                 * Uses a __toString method on the $errors variable which is a
                 * ConstraintViolationList object. This gives us a nice string
                 * for debugging.
                 */
                $errorsString = (string) $errors;

                return new Response($errorsString);
            }

        $user->setApiToken($tokenUser);
        $entityManager->persist($user);

        $entityManager->flush();

        return new JsonResponse(
            [
                'status'    => 'POST OK! GG :)',

                '$emailReceived' => $emailReceived,
                '$passwordReceived' => $passwordReceived,
                

            ]
        );
    }

    


}