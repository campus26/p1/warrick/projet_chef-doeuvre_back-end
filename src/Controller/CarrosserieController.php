<?php

namespace App\Controller;

use App\Entity\Carrosserie;
use App\Entity\Voiture;
use App\Repository\CarrosserieRepository;
use App\Repository\VoitureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/carrosserie")
 */
class CarrosserieController extends AbstractController
{

    private $voitureRepository;

    public function __construct(VoitureRepository $voitureRepository)
    {
        $this->voitureRepository = $voitureRepository;
    }

    /**
     * @Route("/", name="carrosserie_index", methods={"GET"})
     */
    public function index(CarrosserieRepository $carrosserieRepository): Response
    {
        return $this->json($carrosserieRepository->findAll());
    }

    /**
     * @Route("/new", name="carrosserie_new", methods={"POST"})
     */
    public function new(Request $request, ValidatorInterface $validator): Response
    {

        $datas = $request->toArray();

        $cleVoiture = $datas['cle_voiture'];
        if (array_key_exists('facad_av', $datas)) {
            $facadAv = $datas['facad_av'];
        };
        if (array_key_exists('facad_ar', $datas)) {
            $facadAr = $datas['facad_ar'];
        };
        if (array_key_exists('facad_left', $datas)) {
            $facadLeft = $datas['facad_left'];
        };
        if (array_key_exists('facad_right', $datas)) {
            $facadRight = $datas['facad_right'];
        };
        if (array_key_exists('toit', $datas)) {
            $toit = $datas['toit'];
        };

        $entityManager = $this->getDoctrine()->getManager();

        // $voitureRepo = $entityManager->getRepository(Voiture::class);

        //$voiture = $voitureRepo->find(61);

        $carrosserie = new Carrosserie();
        $carrosserie->setCleVoiture($this->voitureRepository->find($cleVoiture));
        if (array_key_exists('facad_av', $datas)) {
            $carrosserie->setFacadAv($facadAv);
        }
        if (array_key_exists('facad_ar', $datas)) {
            $carrosserie->setFacadAr($facadAr);
        }
        if (array_key_exists('facad_left', $datas)) {
            $carrosserie->setFacadLeft($facadLeft);
        }
        if (array_key_exists('facad_right', $datas)) {
            $carrosserie->setFacadRight($facadRight);
        }
        if (array_key_exists('toit', $datas)) {
            $carrosserie->setToit($toit);
        }

        $errors = $validator->validate($carrosserie);
        if (count($errors) > 0) {

            $errorsString = (string) $errors;

            return new Response($errorsString);
        }

        // $carrosserie->setCleVoiture($this->carrosserieRepository->findByVoiture());

        // $carrosserie = $entityManager->getRepository('SdzBlogBundle:Article')->find($id_article);

        // $carrosserie->getCleVoiture();

        $entityManager->persist($carrosserie);
        $entityManager->flush();

        return new JsonResponse(
            [
                'status'    => 'New Carrosserie confirmé !',

                'cle_voiture'    => $cleVoiture,
                'id'  => $carrosserie,
                'datas'     => $datas
            ]
        );
    }

    /**
     * @Route("/{id}", name="carrosserie_show", methods={"GET"})
     */
    public function show(Carrosserie $carrosserie): Response
    {
        return $this->json($carrosserie);
    }

    /**
     * @Route("/edit/{id}", name="carrosserie_edit", methods={"POST"})
     */
    public function edit(Request $request, $id, CarrosserieRepository $carrosserieRepository, ValidatorInterface $validator): Response
    {

        $carrosserie = $carrosserieRepository->findOneBy(['cle_voiture' => $id]);

        $datas = $request->toArray();

        if (array_key_exists('facad_av', $datas)) {
            $facadAv = $datas['facad_av'];
        };
        if (array_key_exists('facad_ar', $datas)) {
            $facadAr = $datas['facad_ar'];
        };
        if (array_key_exists('facad_left', $datas)) {
            $facadLeft = $datas['facad_left'];
        };
        if (array_key_exists('facad_right', $datas)) {
            $facadRight = $datas['facad_right'];
        };
        if (array_key_exists('toit', $datas)) {
            $toit = $datas['toit'];
        };

        $entityManager = $this->getDoctrine()->getManager();

        // $carrosserie = new Carrosserie();

        if (array_key_exists('facad_av', $datas)) {
            $carrosserie->setFacadAv($facadAv);
        }
        if (array_key_exists('facad_ar', $datas)) {
            $carrosserie->setFacadAr($facadAr);
        }
        if (array_key_exists('facad_left', $datas)) {
            $carrosserie->setFacadLeft($facadLeft);
        }
        if (array_key_exists('facad_right', $datas)) {
            $carrosserie->setFacadRight($facadRight);
        }
        if (array_key_exists('toit', $datas)) {
            $carrosserie->setToit($toit);
        }

        // {datas === ['toit' : 'URL']  && $carrosserie->setToit($toit);}

        $errors = $validator->validate($carrosserie);
        if (count($errors) > 0) {

            $errorsString = (string) $errors;

            return new Response($errorsString);
        }

        $entityManager->persist($carrosserie);
        $entityManager->flush();

        return $this->json(
            [
                'status'    => 'Edit / Modif Carrosserie confirmé !',
                'datas'     => $datas
            ]
        );
    }

    /**
     * @Route("/delette/{id}", name="carrosserie_delete", methods={"POST"})
     */
    public function delete(Request $request, Carrosserie $carrosserie): Response
    {
        if ($this->isCsrfTokenValid('delete' . $carrosserie->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($carrosserie);
            $entityManager->flush();
        }

        return new JsonResponse(
            [
                'status'    => 'Delete Carrosserie confirmé !',
            ]
        );
    }
}
