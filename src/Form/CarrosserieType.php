<?php

namespace App\Form;

use App\Entity\Carrosserie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CarrosserieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('facad_av')
            ->add('facad_ar')
            ->add('facad_left')
            ->add('facad_right')
            ->add('toit')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Carrosserie::class,
        ]);
    }
}
