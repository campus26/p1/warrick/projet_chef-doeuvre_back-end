<?php

namespace App\Entity;

use App\Repository\NiveauRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NiveauRepository::class)
 */
class Niveau
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $frein;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $refroidissement;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $direction;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $huile_moteur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reservoir;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pression_pneu;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat_pneu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFrein(): ?string
    {
        return $this->frein;
    }

    public function setFrein(string $frein): self
    {
        $this->frein = $frein;

        return $this;
    }

    public function getRefroidissement(): ?string
    {
        return $this->refroidissement;
    }

    public function setRefroidissement(string $refroidissement): self
    {
        $this->refroidissement = $refroidissement;

        return $this;
    }

    public function getDirection(): ?string
    {
        return $this->direction;
    }

    public function setDirection(string $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    public function getHuileMoteur(): ?string
    {
        return $this->huile_moteur;
    }

    public function setHuileMoteur(string $huile_moteur): self
    {
        $this->huile_moteur = $huile_moteur;

        return $this;
    }

    public function getReservoir(): ?string
    {
        return $this->reservoir;
    }

    public function setReservoir(string $reservoir): self
    {
        $this->reservoir = $reservoir;

        return $this;
    }

    public function getPressionPneu(): ?string
    {
        return $this->pression_pneu;
    }

    public function setPressionPneu(string $pression_pneu): self
    {
        $this->pression_pneu = $pression_pneu;

        return $this;
    }

    public function getEtatPneu(): ?string
    {
        return $this->etat_pneu;
    }

    public function setEtatPneu(string $etat_pneu): self
    {
        $this->etat_pneu = $etat_pneu;

        return $this;
    }
}
