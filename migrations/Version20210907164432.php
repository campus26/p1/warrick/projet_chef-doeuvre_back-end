<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210907164432 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE voiture DROP FOREIGN KEY FK_E9E2810F16BA4BB1');
        $this->addSql('DROP TABLE vitre');
        $this->addSql('ALTER TABLE papier ADD cle_voiture_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE papier ADD CONSTRAINT FK_940A2D5ECF242A6 FOREIGN KEY (cle_voiture_id) REFERENCES voiture (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_940A2D5ECF242A6 ON papier (cle_voiture_id)');
        $this->addSql('ALTER TABLE voiture DROP FOREIGN KEY FK_E9E2810F15BEBE0B');
        $this->addSql('ALTER TABLE voiture DROP FOREIGN KEY FK_E9E2810F42148D31');
        $this->addSql('ALTER TABLE voiture DROP FOREIGN KEY FK_E9E2810F8430F158');
        $this->addSql('ALTER TABLE voiture DROP FOREIGN KEY FK_E9E2810FDE1C5765');
        $this->addSql('DROP INDEX UNIQ_E9E2810F15BEBE0B ON voiture');
        $this->addSql('DROP INDEX UNIQ_E9E2810F16BA4BB1 ON voiture');
        $this->addSql('DROP INDEX UNIQ_E9E2810F42148D31 ON voiture');
        $this->addSql('DROP INDEX UNIQ_E9E2810F8430F158 ON voiture');
        $this->addSql('DROP INDEX UNIQ_E9E2810FDE1C5765 ON voiture');
        $this->addSql('ALTER TABLE voiture DROP cle_interieur_id, DROP cle_niveau_id, DROP cle_mecanique_id, DROP cle_vitre_id, DROP cle_papier_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE vitre (id INT AUTO_INCREMENT NOT NULL, pare_brise_av VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, pare_brise_ar VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, fenetre_d_av VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, fenetre_g_av VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, fenetre_d_ar VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, fenetre_g_ar VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, open_d_av VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, open_g_av VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, open_d_ar VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, open_g_ar VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE papier DROP FOREIGN KEY FK_940A2D5ECF242A6');
        $this->addSql('DROP INDEX UNIQ_940A2D5ECF242A6 ON papier');
        $this->addSql('ALTER TABLE papier DROP cle_voiture_id');
        $this->addSql('ALTER TABLE voiture ADD cle_interieur_id INT DEFAULT NULL, ADD cle_niveau_id INT DEFAULT NULL, ADD cle_mecanique_id INT DEFAULT NULL, ADD cle_vitre_id INT DEFAULT NULL, ADD cle_papier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE voiture ADD CONSTRAINT FK_E9E2810F15BEBE0B FOREIGN KEY (cle_niveau_id) REFERENCES niveau (id)');
        $this->addSql('ALTER TABLE voiture ADD CONSTRAINT FK_E9E2810F16BA4BB1 FOREIGN KEY (cle_vitre_id) REFERENCES vitre (id)');
        $this->addSql('ALTER TABLE voiture ADD CONSTRAINT FK_E9E2810F42148D31 FOREIGN KEY (cle_interieur_id) REFERENCES interieur (id)');
        $this->addSql('ALTER TABLE voiture ADD CONSTRAINT FK_E9E2810F8430F158 FOREIGN KEY (cle_mecanique_id) REFERENCES mecanique (id)');
        $this->addSql('ALTER TABLE voiture ADD CONSTRAINT FK_E9E2810FDE1C5765 FOREIGN KEY (cle_papier_id) REFERENCES papier (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E9E2810F15BEBE0B ON voiture (cle_niveau_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E9E2810F16BA4BB1 ON voiture (cle_vitre_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E9E2810F42148D31 ON voiture (cle_interieur_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E9E2810F8430F158 ON voiture (cle_mecanique_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E9E2810FDE1C5765 ON voiture (cle_papier_id)');
    }
}
